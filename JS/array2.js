/**
 * Created by mizan on 15/03/18.
 */


var arr = [1,2,3,5,6,7];

var newArr = arr.splice(3,1,9,10,11);

console.log("New arr "+newArr);
console.log(arr);

newArr = arr.splice(0,0,9,10,11);

console.log("New arr "+newArr);
console.log(arr);


var nums = [1,4,6,8,10,11];

var allEven = true;

for(let i = 0 ;i < nums.length;i++) {
    if(nums[i]%2 != 0) {
        allEven = false;
        break;
    }
}

if(allEven) {
    console.log("I got all even");
} else {
    console.log("not all nums are even");
}

function checkEven(num) {
    return num%2 == 0;
}

function checkOdd(num) {
    return num%2 != 0;
}

var allEven = nums.every(function(num) {
    return num%2 == 0;
});

allEven ? console.log("I got all even") : console.log("not all nums are even")


var anyOdd = nums.some(function(e){ console.log(e); return e%2 != 0});
anyOdd ? console.log("I got atleats one odd") : console.log(" all nums are even")

