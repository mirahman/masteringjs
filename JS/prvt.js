/**
 * Created by mizan on 07/04/18.
 */


var Person = function  (name) {
    var fullName = "Mastering JS";
    this.name = name;

    return {
        setFullName : function (newValue) {
            console.log('setting new value to '+newValue)
            fullName = newValue;
        },
        getFullName : function () {
            return fullName;
        }
    }; // end of the return
};


var person = new Person('person');
var mizan = new Person('mizan');

console.log(person.getFullName());
person.setFullName( "Jim White" );
console.log(person.getFullName());
mizan.setFullName( "Mizanur rahman" );
console.log(mizan.getFullName());
console.log(person.getFullName());
mizan.IPL = "i am new mizan";
console.log(mizan.getFullName());

console.log(mizan);
console.log(person);
