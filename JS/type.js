/**
 * Created by mizan on 17/02/18.
 */

var a , b;

a = 10;
b = a;

console.log(a);
console.log(b);

a = 20;

console.log(a);
console.log(b);

var x, y;
x = {"name": "mizan"};

y = x;

console.log(x.name);
console.log(y.name);

y.name = "mizanur rahman";

console.log(x.name);
console.log(y.name);