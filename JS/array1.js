/**
 * Created by mizan on 15/03/18.
 */


var arr = [];

function add(number) {
    arr[arr.length] = number;
}

function remove() {
    let num = arr[arr.length-1];
    arr[arr.length-1] = null;
    return num;
}

arr.push(1);
console.log(arr);
arr.push(2);
console.log(arr);
arr.push(3);
console.log(arr);

console.log(arr.pop());
console.log(arr);
console.log(arr.pop());
console.log(arr);
console.log(arr.pop());
console.log(arr);
console.log(arr.pop());
console.log(arr);


//  queue
console.log("Queue");

arr.push(1);
console.log(arr);
arr.push(2);
console.log(arr);
arr.push(3);
console.log(arr);

console.log(arr.shift());
console.log(arr);
console.log(arr.shift());
console.log(arr);
console.log(arr.shift());
console.log(arr);
console.log(arr.shift());
console.log(arr);

var arr = [1,2,3,5,6,7];

var newArr = arr.slice(3,5);

console.log("New slice "+newArr);
console.log(arr);