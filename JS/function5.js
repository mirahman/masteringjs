/**
 * Created by mizan on 08/03/18.
 */


function addCube(a, b, c) {
    function cube(x) {
        return x * x * x;
    }
    return cube(a)+cube(b)+cube(c);
}

console.log(addCube(2,3,4));


function* idGenerator() {
    var id = 1;
    while(id<3) {
        yield id++;
    }
}

var studentID = idGenerator();
var teacherID = idGenerator();

console.log(studentID);

console.log(studentID.next().value);
console.log(teacherID.next().value);
console.log(studentID.next().value);
console.log(teacherID.next().value);
console.log(studentID.next().value);
console.log(studentID.next().value);



function* sampleGenerator() {
    yield console.log(1);
    return console.log(2);
    yield console.log(3);
}

var gen = sampleGenerator();

console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);


var str = "mizanur rahman";
console.log(str.toUpperCase());
console.log(str);

var interest = 7;

function getTotal(amount) {
    //var interest = 5;

    return (amount*(1+interest/100));
}

console.log(getTotal(1000));
interest = 10;
console.log(getTotal(1000));


function showMeAlert() {
    alert("Welcome to class no .....");
    console.log('showing alert');
}

function showMeTime() {
    console.log("Time is "+new Date());
}

setTimeout(showMeAlert, 10000);

setInterval(showMeTime, 5000);

console.log('alert is called');
