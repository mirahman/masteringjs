/**
 * Created by mizan on 17/03/18.
 */

var obj = {
            name: "mizan",
            age: 38,
            address: {
                road: 10,
                house: 27,
                area: 'bashundhara r/a',
                city: 'dhaka'
            }
};

var obj2 = JSON.parse(JSON.stringify(obj));

var obj1 = Object.assign({}, obj);

console.log(obj1);
obj1.name = "niloy";
obj1.address.road = 20;
obj1.address.city = 'Khulna';

console.log(JSON.stringify(obj1));
console.log(JSON.stringify(obj));
console.log(JSON.stringify(obj2));

console.log(obj1);
console.log(obj);
console.log(obj2);