/**
 * Created by mizan on 16/03/18.
 */


function Student(name, roll) {
    this.name = name;
    this.roll = roll;

    this.showName = function() {
        return "My name is "+this.name;
    }

}

var shahan = new Student('Shahan','1');
console.log(shahan.showName());

var niloy = Object.create(shahan);

niloy.name = "niloy";

console.log(niloy);
console.log(shahan);

var proto = {
    describe: function () {
        return 'name: '+this.name;
    }
};

var obj = {
    name: 'obj'
};

obj.__proto__ = proto;

console.log(obj.describe())