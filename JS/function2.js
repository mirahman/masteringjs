/**
 * Created by mizan on 03/03/18.
 */


var a = 10;


function sum(){
    a = 20;
    console.log(a);
}


function sum(a, b , c) {

    console.log(arguments[4]);
    a = a + b + c;
    return a;
}


console.log("A is "+a);
console.log(sum(10,20,30,40,50,60));
console.log("A is "+a);


function sumAll(...args) {
    var sum = 0;
    for(let i of args) {
        sum += i;
    }

    return sum;
}

console.log(sumAll(1))

console.log(Math.max(1,3,-5,8));

var userVotes = [1,3,4,6,7,9,90,87,65];

console.log(Math.max(...userVotes));


var a = 10;
var b = 20;

function doSomething(x, y) {
    x = x * 2;
    y = x + y;
}

function doSomethingWithArray(x) {
    x[0] *= 2;
    x[1] *= 4;
}


console.log("a = "+a);
console.log("b = "+b);
doSomething(a, b);
console.log("a = "+a);
console.log("b = "+b);

var arr = [1,2];
console.log(arr);
doSomethingWithArray(arr);
console.log(arr);