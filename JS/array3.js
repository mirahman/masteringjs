/**
 * Created by mizan on 15/03/18.
 */

var nums = [1,4,6,8,10,11,13,16,22,23,1];

nums.forEach(function(e){console.log(e)});

var sum = nums.filter(function(e){
    return e%2 == 0;
}).reduce(function(prev, current){
    return prev+current;
});

console.log("sorted :"+nums.sort(function(a, b){ return a > b ? -1 : (a < b ? 1 : 0);}));

var sum = nums.filter(e => e%2 == 0).reduce((prev, current) => prev + current);




console.log(sum);

console.log(nums);

var nums = [1,4,6,8,10,11,13,16,22,23,1];

var newArr = nums.map( e => e*3);

console.log(newArr);
console.log(nums);

var nums = Array(10);
console.log(nums);
var test = nums.fill(5);
console.log(test);

console.log(test.join(" / "))

function logArrayElements(element, index, array) {
    console.log('a[' + index + '] = ' + element+" "+array);
}

// Notice that index 2 is skipped since there is no item at
// that position in the array.
[2, 5, , 9].forEach(logArrayElements);

var a = 5;
var b = 10;
console.log(`Fifteen is ${a + b} and not ${2 * a + b}.`);