var obj = {
    name: "mizan",
    age: 38,
    address: {
        house: 27,
        road: 10,
        block: 'b'
    }
}

obj.degree = "BS in Computer Science";

Object.defineProperty(obj, 'degree', {
    value: "BS in Computer Science",
    enumerable: false,
    configurable: false
});


Object.defineProperty(obj, 'degree', {
    value: "BS in Computer Science",
    writable: false
});

console.log(Object.keys(obj));

function isReadOnly(objName, propertyName) {
  return !Object.getOwnPropertyDescriptor(objName,propertyName).writable
}

console.log(isReadOnly(obj, 'degree'));

Object.defineProperty(obj, 'address', {
    value: {
        house: 27,
        road: 10,
        block: 'b'
    },
    writable: false
});

obj.address.block = 'D';

console.log(obj);