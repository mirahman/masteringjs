/**
 * Created by mizan on 16/03/18.
 */


function student(name, age, roll) {
    this.name = name;
    this.age = age;
    this.roll = roll;

}

var shahan = new student("shahan",24,1);
var niloy = new student("Niloy",22,2);

niloy.name = "name";

shahan.getName(niloy);

var student = {
    name: '',
    age: '',
    roll: ''
}

console.log(shahan.name+ " "+niloy.name);


var person1 = {fullname:'shahan',roll:'20'};
var person2 = {fullname:'niloy',roll:'24'};

function welcome(greeting) {
    console.log(greeting+': '+this.fullname+" and your age is "+this.roll);
}

welcome.call(person1, "Hello");

