/**
 * Created by mizan on 10/03/18.
 */



var a = 10;
var b = 0;

//console.log(c);

try {

    console.log(10);
    //console.log(c);

    if(b<=0)
        throw new RangeError("value must be greater than 0");

    console.log(a/b);
    console.log('done with divide');
} catch(e) {
    if(e instanceof RangeError)
        console.log('found range error '+e.message);
    else if(e instanceof ReferenceError)
        console.log('found reference error '+e.message);
    else
        console.log("general error");

}
finally {
    console.log('finally Done');
}

//close the connection

try {

    try {

        console.log("internal "+10);
        console.log(c);

        if(b<=0)
            throw new RangeError("internal value must be greater than 0");

        console.log(a/b);
        console.log('internal done with divide');
    }
    finally {
        console.log('internal finally Done');
    }

    console.log("done with internal try");

} catch(e) {
    if(e instanceof RangeError)
        console.log('found range error '+e.message);
    else if(e instanceof ReferenceError)
        console.log('found reference error '+e.message);
    else
        console.log("general error");

}
finally {
    console.log('finally Done');
}