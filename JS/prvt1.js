/**
 * Created by mizan on 07/04/18.
 */

function Container(param) {

    function decrement() {
        if (secret > 0) {
            secret -= 1;
            return true;
        } else {
            return false;
        }
    }

    // privileged method
    this.doDec = function() {
        decrement();
    }

    this.member = param;
    var secret = 3;
    var that = this;

    this.service = function () {
        return decrement() ? that.member : "no value";
    };
}


var a = new Container("mizan");
a.doDec();
//console.log(a.member);
console.log(a.service());
console.log(a.service());
console.log(a.service());
console.log(a.service());

var packageName = "Mizan JS class";

var packageName = "Shahan's master class";