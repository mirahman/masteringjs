/**
 * Created by mizan on 17/03/18.
 */

class Person {

    constructor(first, last, age, gender) {
        this.first = first;
        this.last = last;
        this.age = age;
        this.gender = gender;
    }


    get fullName() {
        return this.first+" "+this.last;
    }

    set fullName(fname) {
        this.first = fname.split()[0];
        this.last = lname;
    }

    showFullName() {
        return this.first+" "+this.last;
    }

    static showMeSomething() {
        return "I am from Person";
    }
}


var mizan = new Person("Mizanur", "Rahman", 38, "M");

console.log(mizan.fullName)
console.log(mizan.showFullName())
console.log(Person.showMeSomething());

mizan.fullName = "mizanur rahman";

class Teacher extends Person {


    constructor(first, last, age, gender, subject) {
        super(first,last, age, gender);
        this.subject = subject;
    }

}

var tech = new Teacher("Mizanur", "Rahman", 38, "M","Mastering JS");
console.log(tech);