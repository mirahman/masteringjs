/**
 * Created by mizan on 16/03/18.
 */


var person1 = {fullname:'shahan',roll:'20'};
var person2 = {fullname:'niloy',roll:'24'};

function welcome(greeting) {
    console.log(greeting+': '+this.fullname+" and your age is "+this.roll);
}

var func1 = welcome.bind(person1);
var func2 = welcome.bind(person2);

console.log(func1);

func1('Hi');
func2('welcome');

welcome.apply(person1,['Hi there','How are you']);


var student = {
    fname: 'niloy',
    lname: 'ahmed',
    age: 22,


    get fullName() {
        return this.fname+" "+this.lname;
    },


    set fullName(val) {
        let t = val.split(" ");
        console.log(t);
        this.fname = t[0];
        this.lname = t[1];
    }
}
student.fname = "mizan";
console.log(student.fullName);


student.fullName = "Niloy rahman";

console.log(student);

