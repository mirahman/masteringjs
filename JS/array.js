/**
 * Created by mizan on 14/03/18.
 */

var arr = [10, 20, 30];
var arr1 = Array(10);

console.log(arr[2]);

arr[2] = 100;

arr[25] = 10;

console.log(arr[2]);


console.log(arr+ " > "+arr1);
console.log(arr.length+ " "+arr1['length']);


var sayings = new Map();
sayings.set('dog', 'woof');
sayings.set('cat', 'meow');
sayings.set('elephant', 'toot');
sayings.set('bird','chrip');
sayings.delete('dog');
sayings.set('dog', 'gheu ghew');
sayings.size; // 3
/*
sayings.get('fox'); // undefined
sayings.has('bird'); // false
sayings.delete('dog');
sayings.has('dog'); // false
*/

for (var [key, value] of sayings) {
    console.log(key + ' goes ' + value);
}
// "cat goes meow"
// "elephant goes toot"

sayings.clear();
sayings.size; // 0


