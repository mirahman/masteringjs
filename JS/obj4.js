/**
 * Created by mizan on 17/03/18.
 */


var animal = {
    name: 'animal',
    walk: function() {
        console.log('Animal can walk '+this.name);
    }
}

var human = {
    name: 'human',
    talk: function() {
        console.log('Human can talk '+this.name);
    }
}

var avengers = {
    name: 'super man',
    fly: function () {
        console.log('I can fly '+this.name);
    }
}

var ironMan = Object.create(avengers);
console.log(ironMan);

human.__proto__ = animal;
avengers.__proto__ = human;

avengers.talk();
avengers.walk();

animal.talk();