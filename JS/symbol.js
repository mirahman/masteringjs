/**
 * Created by mizan on 07/04/18.
 */


var id = Symbol('name');
console.log(id);
var id1 = Symbol('name');
console.log(id1);

console.log(id == id1);

let Person = {name: "default", [id]: 10};

Person.name = "mizan";
Person[id] = "Mizanur Rahman";
console.log(Person[id]);
Person[id1] = "Mizanur Rahman rubel";
Person[id] = "Mizanur Rahman - (Rubel)";
console.log(Person[id1]);
console.log(Person[id]);


let idRepeat = Symbol.for("name");

var id2 = Symbol('name');

/pattern/flag