/**
 * Created by mizan on 17/03/18.
 */

function Person(first, last, age, gender) {
    this.first = first;
    this.last = last;
    this.age = age;
    this.gender = gender;

    this.anotherShow = function() {
        console.log('SHowing another name '+this.first);
    }
};

console.log(Person);
console.log(Person.prototype);
console.log(Person.prototype.constructor);
console.log(Person.__proto__);

Person.prototype.greeting = function() {
    console.log('Hi! I\'m ' + this.first + '.');
};


var shahan = new Person("shahan","bhai", 24, "M");
var niloy  = new Person("Niloy", "bhai", 22, "M");

shahan.anotherShow();

function Teacher(first, last, age, gender, subject) {
    Person.call(this, first, last, age, gender);
    this.subject = subject;
}

var mizan = new Teacher('mizan','rahman',37,'male','php');

Teacher.prototype.greeting = Person.prototype.greeting;

mizan.anotherShow();


Teacher.prototype = Object.create(Person.prototype);
Teacher.prototype.constructor = Teacher;

console.log(Teacher.prototype.constructor);