/**
 * Created by mizan on 16/03/18.
 */


function Student(name, roll) {
    this.name = name;
    this.roll = roll;

    this.showName = function() {
        return "My name is "+this.name;
    }

}

var shahan = new Student('Shahan','1');
console.log(shahan.showName());

var niloy = new Student("Niloy",'2');



Student.prototype.newShowName = function(){
    console.log("showing new show name funciton");
}

shahan.newShowName();

niloy.newShowName();