/**
 * Created by mizan on 14/03/18.
 */

function resolveAfter2Seconds(x) {
    return new Promise(resolve => {
        setTimeout(() => {
        resolve(x);
    }, 2000);
});
}


async function add1(x) {
    const a = await resolveAfter2Seconds(20);
    console.log("i am creating b");
    const b = await resolveAfter2Seconds(a);
    console.log("i am done with b");
    const c = await resolveAfter2Seconds(b);
    return x + a + b + c;
}


add1(10).then(v => {
    console.log(v);  // prints 60 after 4 seconds.
});
console.log("i am done with calling");

add1(30).then(v => {
    console.log(v);  // prints 60 after 4 seconds.
});
console.log("i am done with calling");

