/**
 * Created by mizan on 02/03/18.
 */

/*
for(var X = 1;X<=10;X++) {
    for (var x = 1; x <= 10; x++) {
        console.log(X + " X " + x + " = " + (x * X));
    }
}
*/
var count = 0;
level0:
for(var x = 0;x<5;x++) {
    level1: {
        console.log("Entering level 1 with "+x);
        for (var a = 1; a <= 10; a++) {
            level2:
                for (var b = 1; b <= 10; b++) {
                    count++;
                    if (a <= b) break level0;

                    level3:
                        for (var c = 1; c <= 10; c++) {
                            console.log(a + " > " + b + " > " + c);

                        }
                }
        }

    }
}

console.log("count is "+count);


var arr = [1,2,3,4,5];

for(let i in arr) {
    console.log(i+" "+arr[i]);
}



var a = true;
while(a) {


    a = false;
}

do {

    a = false;
} while(a);