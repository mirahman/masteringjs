/**
 * Created by mizan on 10/03/18.
 */

var iAmKeepingPromise = false;

var testPromise = new Promise(function(resolve, reject){


    //reject('i kept my promise');



    if(iAmKeepingPromise) {
        resolve("i kept my promise");
    } else {
        reject("i do not want to keep it");
    }


})


var execPromise = function() {

    testPromise.then(function(msg){
            console.log("Promise kept with "+msg);
    }).catch(function(msg) {
            console.log('Promise rejected reason '+msg);
    });

}

execPromise();

async function newExec() {
    try {
        var message = await testPromise;
        console.log(message);
    } catch(e) {
        console.log('error occured '+e);
    }
}

async function anotherFunction(obj) {
    try {
        console.log(obj);
    } catch(e) {
        console.log('error occured '+e);
    }
}



anotherFunction(test);

